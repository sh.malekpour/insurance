import React from "react";
import { Form, Select ,Input } from "antd";

const { Option } = Select;
const ReminderFormInput = () => {
  const [form] = Form.useForm();
  const onFinish = (values) => {
    console.log(values);
  };

  return (
    <div className="reminder-form__container">
      <div className="flex flex-row justify-center">
        <img alt="" src="/images/announceright.png" />
        <div className="reminder-form__title">
          <h3>یادآور تمدید یا اقساط انواع بیمه نامه ها</h3>
        </div>
        <img alt="" src="/images/announceleft.png" />
      </div>
      <div className="flex">
        <Form
          form={form}
          name="control-hooks"
          onFinish={onFinish}
          className="w-full"
        >
          <div className="grid col-gap-10 grid-cols-4">
            <Form.Item name="insurance-type">
              <Select placeholder="نوع بیمه" allowClear>
                <Option value="iran">ایرانخودرو</Option>
                <Option value="sipa">سایپا</Option>
                <Option value="mvm">ام وی ام</Option>
              </Select>
            </Form.Item>
            <Form.Item name="expire-date">
              <Select placeholder="تاریخ سررسید" allowClear>
                <Option value="ten-percent">10</Option>
                <Option value="fifty-percent">50</Option>
                <Option value="sixty-percent">60</Option>
              </Select>
            </Form.Item>
            <Input placeholder="نام و نام خانوادگی" type="text"  />
            <Input placeholder="شماره موبایل" type="number" />
          </div>
        </Form>
      </div>
      <div className="flex flex-row justify-center">
        <button className="reminder-form__btn">ثبت اطلاعات</button>
      </div>
    </div>
  );
};

export default ReminderFormInput;

import React from "react";

const navData = [
  { id: 1, link: "#home", title: "خانه" },
  { id: 2, link: "#thied-party-insurance", title: "بیمه شخص ثالث" },
  { id: 3, link: "#body-insurance", title: "بیمه بدنه" },
  { id: 4, link: "#persons-insurance", title: "بیمه اشخاص" },
  { id: 5, link: "#insurance-company", title: "شرکت های بیمه" },
];
const NavBar = () => {
  return (
    <nav>
      <ul className="navbar">
        {navData.map((item) => (
          <li key={item.id}>
            <img alt="home" src="/images/home.png" />
            <a href={item.link} alt={item.title}>
              {item.title}
            </a>
          </li>
        ))}
      </ul>
    </nav>
  );
};

export default NavBar;

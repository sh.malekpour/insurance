import React from "react";

const icons = [
  { id: 1, image: "/images/earthquake.svg" },
  { id: 2, image: "/images/fire.svg" },
  { id: 3, image: "/images/motorbike.svg" },
  { id: 4, image: "/images/insurance.svg" },
  { id: 5, image: "/images/fender-bender.svg" },
  { id: 6, image: "/images/travel-insurance.svg" },
  { id: 7, image: "/images/human-insurance.svg" },
];

const Hero = () => {
  return (
    <div>
      {icons.map((icon) => (
        <div className="circle-icon" key={icon.id}>
          <img alt="" src={icon.image} />
        </div>
      ))}
    </div>
  );
};

export default Hero;

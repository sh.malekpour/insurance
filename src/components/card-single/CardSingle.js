import React from "react";


const CardSingle = ({item}) => {
  return (
    <div className="card-container rounded overflow-hidden">
      <img
        className="w-full"
        src={`/images/${item.image}`}
        alt={item.title}
      />
      <div className="px-6 py-4">
        <div className="font-bold text-xl mb-2">{item.title}</div>
        <p className="card-description">
        {item.description}
        </p>
      </div>
      <div className="px-6 pt-4 pb-2 text-left">
        <span className="inline-block text-sm font-semibold text-gray-700">
         ادامه مطلب
        </span>
        
      </div>
    </div>
  );
};

export default CardSingle;

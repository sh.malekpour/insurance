import React from "react";
import { Form, Select } from "antd";

const { Option } = Select;

const InsuranceFormInput = () => {
  const [form] = Form.useForm();
  const onFinish = (values) => {
    console.log(values);
  };

  return (
    <div className="insurance-form__container">
      <div className="form-title">بیمه شخص ثالث</div>
      <div className="flex justify-end">
        <div className="info-btn">پرداخت اقساطی</div>
      </div>
      <div className="flex">
        <Form
          form={form}
          name="control-hooks"
          onFinish={onFinish}
          className="w-full"
        >
          <div className="grid col-gap-10 grid-cols-3 ">
            <Form.Item
              name="car-type"
              label="نوع وسیله نقلیه"
              labelCol={{ span: 24 }}
              labelAlign="right"
            >
              <Select placeholder="انتخاب برند خودرو" allowClear>
                <Option value="iran">ایرانخودرو</Option>
                <Option value="sipa">سایپا</Option>
                <Option value="mvm">ام وی ام</Option>
              </Select>
            </Form.Item>
            <div className="flex flex-row">
              <div className="w-3/5">
                <Form.Item
                  name="date"
                  label="تاریخ سر رسید"
                  labelCol={{ span: 24 }}
                  labelAlign="right"
                >
                  <Select placeholder="انتخاب تاریخ" allowClear>
                    <Option value="iran">ایرانخودرو</Option>
                    <Option value="sipa">سایپا</Option>
                    <Option value="mvm">ام وی ام</Option>
                  </Select>
                </Form.Item>
              </div>
              <div className="w-1/3 mr-auto">
                <label className="form-switch">
                  <div className="form-switch__label">بیمه نامه خسارت</div>
                  <input type="checkbox" />
                  <i></i>
                </label>
              </div>
            </div>
            <Form.Item
              name="driver-discount"
              label="درصد تخفیف حوادث راننده"
              labelCol={{ span: 24 }}
              labelAlign="right"
            >
              <Select placeholder="تخفیف حوادث راننده" allowClear>
                <Option value="ten-percent">10</Option>
                <Option value="fifty-percent">50</Option>
                <Option value="sixty-percent">60</Option>
              </Select>
            </Form.Item>
            <Form.Item
              name="year"
              label="سال ساخت"
              labelCol={{ span: 24 }}
              labelAlign="right"
            >
              <Select placeholder="انتخاب سال ساخت" allowClear>
                <Option value="1390">1390</Option>
                <Option value="1395">1395</Option>
                <Option value="1399">1399</Option>
              </Select>
            </Form.Item>
            <Form.Item
              name="car-model"
              label="مدل خودرو"
              labelCol={{ span: 24 }}
              labelAlign="right"
            >
              <Select placeholder="انتخاب مدل خودرو" allowClear>
                <Option value="model1">10</Option>
                <Option value="model2">50</Option>
                <Option value="model3">60</Option>
              </Select>
            </Form.Item>
            <Form.Item
              name="insurance-discount"
              label="درصد تخفیف بیمه نامه قبلی"
              labelCol={{ span: 24 }}
              labelAlign="right"
            >
              <Select placeholder="تخفیف عدم خسارت" allowClear>
                <Option value="ten-discount">10</Option>
                <Option value="fifty-discount">50</Option>
                <Option value="sixty-discount">60</Option>
              </Select>
            </Form.Item>
          </div>
        </Form>
      </div>
    </div>
  );
};

export default InsuranceFormInput;

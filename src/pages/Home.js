import React from "react";
import HeroHeader from "containers/hero-header";
import InsuranceForm from "containers/insurance-form";
import Features from "containers/features";
import ReminderForm from "containers/reminder-form";
import Card from "containers/card"
const Home = () => {
  return (
    <React.Fragment>
      <HeroHeader />
      <InsuranceForm />
      <Features />
      <ReminderForm/>
      <Card/>
    </React.Fragment>
  );
};

export default Home;

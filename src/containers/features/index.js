import React from "react";

const featureItems = [
  { id: 1, title: "مشاوره تخصصی بیمه", image: "telephone.svg" },
  { id: 2, title: "مقایسه قیمت بیمه", image: "analytics.svg" },
  { id: 3, title: "ارسال فوری", image: "free-delivery.svg" },
  { id: 4, title: "خرید سریع و آسان", image: "credit-card.svg" },
];
const Features = () => {
  return (
    <section className="features-area">
      <div className="features-container">
        <div className="grid grid-cols-4 gap-4">
          {featureItems.map((feature) => (
            <div key={feature.id}>
              <div className="feature-icon">
                <img alt="" src={`/images/${feature.image}`} />
              </div>
              <div className="feature-title">
                <h3>{feature.title}</h3>
              </div>
            </div>
          ))}
        </div>
      </div>
    </section>
  );
};

export default Features;

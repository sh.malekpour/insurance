import React from "react";
import Hero from "components/hero-header/Hero";
const HeroHeader = () => {
  return (
    <div className="hero-header-area">
      <div className="info-area">
        <img alt="" src="/images/loginlogobime.svg" className="logo-img" />
        <Hero />
      </div>
      
    </div>
  );
};

export default HeroHeader;

import React, { useState, useEffect } from "react";
import NavBar from "components/header/NavBar";

const Header = () => {
  const [scroll, setScroll] = useState(0);
  const [headerTop, setHeaderTop] = useState(0);

  useEffect(() => {
    const header = document.querySelector(".header-area");
    setHeaderTop(header.offsetTop);
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);
  const handleScroll = () => {
    setScroll(window.scrollY);
  };
  return (
    <header
      className={`header-area flex flex-row justify-between ${
        scroll > headerTop ? "stick" : ""
      }`}
    >
      <div className="flex flex-row">
        <NavBar />
      </div>
      <div className="flex">
        <button className="login-btn">
          <img alt="" src="/images/home.png" />
          ورود / ثبت نام
        </button>
      </div>
    </header>
  );
};

export default Header;

import React from "react";
import InsuranceFormInput from "components/insurance-form/InsuranceFormInput";
const InsuranceForm = () => {
  return (
    <section className="insurance-form__area">
      <img alt="" src="/images/square.png" />
      <InsuranceFormInput />
      <div className="flex justify-center">
        <button className="insurance-form__btn">استعلام قیمت</button>
      </div>
    </section>
  );
};

export default InsuranceForm;

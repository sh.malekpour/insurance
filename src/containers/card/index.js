import React from "react";
import CardSingle from "components/card-single/CardSingle";

const cardItems = [
  {
    id: 1,
    title: "بیمه آتش سوزی",
    description:
      "بیمه ‌آتش‌سوزی یکی از قدیمی‌ترین رشته‌های بیمه در دنیا است. اهمیت این بیمه تا آنجا است که در کشورهای توسعه‌یافته، تقریباً هیچ دارایی یا مالی را نمی‌توان یافت که بیمه آتش‌سوزی نداشته باشد.",
    image: "elijah.svg",
  },
  {
    id: 2,
    title: "بیمه زلزله",
    description:
      "بیمه ‌آتش‌سوزی یکی از قدیمی‌ترین رشته‌های بیمه در دنیا است. اهمیت این بیمه تا آنجا است که در کشورهای توسعه‌یافته، تقریباً هیچ دارایی یا مالی را نمی‌توان یافت که بیمه آتش‌سوزی نداشته باشد.",
    image: "noaa.svg",
  },
  {
    id: 3,
    title: "بیمه بدنه",
    description:
      "بیمه ‌آتش‌سوزی یکی از قدیمی‌ترین رشته‌های بیمه در دنیا است. اهمیت این بیمه تا آنجا است که در کشورهای توسعه‌یافته، تقریباً هیچ دارایی یا مالی را نمی‌توان یافت که بیمه آتش‌سوزی نداشته باشد.",
    image: "takahiro.svg",
  },
  {
    id: 4,
    title: "بیمه شخص ثالث",
    description:
      "بیمه ‌آتش‌سوزی یکی از قدیمی‌ترین رشته‌های بیمه در دنیا است. اهمیت این بیمه تا آنجا است که در کشورهای توسعه‌یافته، تقریباً هیچ دارایی یا مالی را نمی‌توان یافت که بیمه آتش‌سوزی نداشته باشد.",
    image: "tim-mossholder.svg",
  },
];

const Card = () => {
  return (
    <section className="card-area">
     <div className="grid grid-cols-4 col-gap-8">
     {cardItems.map((item) => (
        <CardSingle key={item.id} item={item} />
      ))}
     </div>
    </section>
  );
};

export default Card;

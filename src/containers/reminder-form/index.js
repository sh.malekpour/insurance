import React from "react";
import ReminderFormInput from "components/reminder-form/ReminderFormInput";
const ReminderForm = () => {
  return (
    <section className="reminder-form__area">
      <div className="reminder-form__image">
        <img alt="" src="/images/square.png" className="square-img" />
      </div>
      <ReminderFormInput />
    </section>
  );
};

export default ReminderForm;

import React from "react";
import Home from "pages/Home";
import Header from "containers/header";

function App() {
  return (
    <React.Fragment>
      <Header />
      <Home />
    </React.Fragment>
  );
}

export default App;
